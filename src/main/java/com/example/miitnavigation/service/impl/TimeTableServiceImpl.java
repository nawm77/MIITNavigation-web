package com.example.miitnavigation.service.impl;

import com.example.miitnavigation.model.TimeTable;
import com.example.miitnavigation.repository.TimeTableRepository;
import com.example.miitnavigation.service.AuditoriumService;
import com.example.miitnavigation.service.TimeTableService;
import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;

@Service
@Transactional
public class TimeTableServiceImpl implements TimeTableService {
    @PersistenceContext
    private EntityManager entityManager;
    private final TimeTableRepository timeTableRepository;

    @Autowired
    public TimeTableServiceImpl(TimeTableRepository timeTableRepository) {
        this.timeTableRepository = timeTableRepository;
    }

    public void saveTimeTable(TimeTable timeTable) {
        entityManager.merge(timeTable);
    }


    @Override
    public List<TimeTable> findAllWithFetch() {
        return timeTableRepository.findAllWithFetch();
    }

    @Async
    @Override
    public CompletableFuture<TimeTable> createTimeTable(TimeTable timeTable) {
        return CompletableFuture.completedFuture(timeTableRepository.saveAndFlush(timeTable));
    }

    @Async
    @Override
    public CompletableFuture<Optional<TimeTable>> getTimeTableById(Long id) {
        return CompletableFuture.completedFuture(timeTableRepository.findById(id));
    }

    @Async
    @Override
    public CompletableFuture<List<TimeTable>> getTimeTable() {
        return CompletableFuture.completedFuture(timeTableRepository.findAll());
    }

    @Override
    public CompletableFuture<Void> dropTimeTable() {
        timeTableRepository.deleteAll();
        return CompletableFuture.completedFuture(null);
    }

    @Async
    @Override
    public CompletableFuture<Void> deleteTimeTableById(Long id) {
        timeTableRepository.deleteById(id);
        return CompletableFuture.completedFuture(null);
    }
}
